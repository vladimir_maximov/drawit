#include "paintwidget.h"
#include <QDebug>

void PaintWidget::paintEvent(QPaintEvent*)
{
    QPainter p(this);
    painter->paint(&p, width(), height());
}
