#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QWidget>
#include "painter.h"

class PaintWidget : public QWidget
{
    Q_OBJECT

private:
    Painter *painter;

protected:
    void paintEvent(QPaintEvent*);

public:
    explicit PaintWidget(Painter *painter, QWidget *parent = 0)
        : painter(painter), QWidget(parent) {}

signals:

public slots:
};

#endif // PAINTWIDGET_H
