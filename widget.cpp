#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    painter = new Painter();

    r1Spin = new QSpinBox;
    r1Spin->setValue(painter->r1());
    r1Spin->setFixedSize(80, 28);

    r2Spin = new QSpinBox;
    r2Spin->setValue(painter->r2());
    r2Spin->setFixedSize(80, 28);

    r3Spin = new QSpinBox;
    r3Spin->setValue(painter->r3());
    r3Spin->setFixedSize(80, 28);

    drawBtn = new QPushButton("Draw");
    drawBtn->setFixedSize(140, 30);
    connect(drawBtn, SIGNAL(clicked()), SLOT(onDrawClicked()));

    QHBoxLayout *hbl = new QHBoxLayout;
    hbl->addStretch(1);
    hbl->addWidget(new QLabel("R1"));
    hbl->addWidget(r1Spin);
    hbl->addSpacing(10);
    hbl->addWidget(new QLabel("R2"));
    hbl->addWidget(r2Spin);
    hbl->addSpacing(10);
    hbl->addWidget(new QLabel("R3"));
    hbl->addWidget(r3Spin);
    hbl->addSpacing(10);
    hbl->addWidget(drawBtn);
    hbl->addStretch(1);

    paintWidget = new PaintWidget(painter);

    QVBoxLayout *vbl = new QVBoxLayout;
    vbl->addLayout(hbl);
    vbl->addWidget(paintWidget, 1);

    setLayout(vbl);

    resize(500, 500);
}

Widget::~Widget()
{
    delete painter;
}

void Widget::onDrawClicked()
{
    painter->update(r1Spin->value(), r2Spin->value(), r3Spin->value());
    paintWidget->update();
}
