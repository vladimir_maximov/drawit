#-------------------------------------------------
#
# Project created by QtCreator 2016-03-03T15:46:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DrawIt
TEMPLATE = app

CONFIG += c++11


SOURCES += main.cpp\
        widget.cpp \
    paintwidget.cpp \
    painter.cpp

HEADERS  += widget.h \
    paintwidget.h \
    painter.h
