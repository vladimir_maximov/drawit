#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QSpinBox>
#include <QBoxLayout>
#include <QPushButton>
#include <QLabel>

#include "paintwidget.h"

class Widget : public QWidget
{
    Q_OBJECT

    QSpinBox *r1Spin;
    QSpinBox *r2Spin;
    QSpinBox *r3Spin;
    QPushButton *drawBtn;
    PaintWidget *paintWidget;

    Painter *painter;

public:
    Widget(QWidget *parent = 0);
    ~Widget();

public slots:
    void onDrawClicked();
};

#endif // WIDGET_H
