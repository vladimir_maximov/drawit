#include "painter.h"
#include <QDebug>

void Painter::update(int r1, int r2, int r3)
{
    _r1 = r1;
    _r2 = r2;
    _r3 = r3;
    k = _r1 + _r2;
    m = _r2 ? k / _r2 : 0;

    r1Circle.clear();
    r2Circle.clear();
    path.clear();

    qreal delta = (tmax - tmin) / iterations;
    qreal t = tmin;

    qreal xmin, xmax, ymin, ymax;
    QPointF p1, p2, p3;
    calc(tmin, p1, p2, p3);

    xmin = min3(p1.x(), p2.x(), p3.x());
    ymin = min3(p1.y(), p2.y(), p3.y());
    xmax = max3(p1.x(), p2.x(), p3.x());
    ymax = max3(p1.y(), p2.y(), p3.y());

    // Вычисление полигонов и минимаксов
    //
    int i = 0;
    while (i < iterations) {
        t = i++ * delta;
        calc(t, p1, p2, p3);

        r1Circle.append(p1);
        r2Circle.append(p2);
        path.append(p3);

        xmin = qMin(min3(p1.x(), p2.x(), p3.x()), xmin);
        ymin = qMin(min3(p1.y(), p2.y(), p3.y()), ymin);
        xmax = qMax(max3(p1.x(), p2.x(), p3.x()), xmax);
        ymax = qMax(max3(p1.y(), p2.y(), p3.y()), ymax);

    }

    // Сдвиг в 0
    //
    for (i = 0; i < iterations; i++) {
        r1Circle[i].setX(r1Circle.at(i).x() - xmin);
        r2Circle[i].setX(r2Circle.at(i).x() - xmin);
        path[i].setX(path.at(i).x() - xmin);

        r1Circle[i].setY(r1Circle.at(i).y() - ymin);
        r2Circle[i].setY(r2Circle.at(i).y() - ymin);
        path[i].setY(path.at(i).y() - ymin);
    }

    // Вычисление ограничивающего прямоугольника
    //
    xlen = qAbs(xmax - xmin);
    ylen = qAbs(ymax - ymin);
}

void Painter::paint(QPainter *p, qreal width, qreal height)
{
    QPolygonF p1(r1Circle);
    QPolygonF p2(r2Circle);
    QPolygonF p3(path);

    // Коеф. отношений длин сторон
    //
    qreal widthCoef = width/xlen;
    qreal heightCoef = height/ylen;

    // Вычисление фактора масштабирования и сдвигов для центрирования
    //
    qreal scaleFactor = widthCoef > heightCoef ? heightCoef : widthCoef;
    qreal xOffset = (width - xlen * scaleFactor) / 2;
    qreal yOffset = (height - ylen * scaleFactor) / 2;


    // Масштабирование и центрирование
    //
    for (int i = 0; i < iterations; i++) {
        p1[i].setX(p1.at(i).x()*scaleFactor + xOffset);
        p2[i].setX(p2.at(i).x()*scaleFactor + xOffset);
        p3[i].setX(p3.at(i).x()*scaleFactor + xOffset);

        p1[i].setY(p1.at(i).y()*scaleFactor + yOffset);
        p2[i].setY(p2.at(i).y()*scaleFactor + yOffset);
        p3[i].setY(p3.at(i).y()*scaleFactor + yOffset);
    }

    // Отрисовка
    //
    p->fillRect(0, 0, width, height, QColor("#FCFCFC"));
    p->setPen("red");
    p->drawPolygon(p1);
    p->setPen("green");
    p->drawPolygon(p2);
    p->setPen("blue");
    p->drawPolyline(p3);
}

void Painter::calc(qreal t, QPointF &p1, QPointF &p2, QPointF &p3)
{
    p1 = QPointF(_r1 + _r1*cos(t), _r1*sin(t));
    p2 = QPointF(2*_r1 + _r2 * (1+cos(t)), _r2*sin(t));
    p3 = _r2 ? QPointF(_r1 +  k*cos(t) + _r3*cos(m*t), k*sin(t) + _r3*sin(m*t))
             : QPointF(0, 0);
}
