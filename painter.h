#ifndef PAINTER_H
#define PAINTER_H

#include <QPainter>
#include <qmath.h>

// Отрисовка траектории, целый класс :)
//
class Painter
{
    qreal _r1;  // радиус фикс. окр-ти
    qreal _r2;  // радиус движущейся окр-ти
    qreal _r3;  // расстояние от центра движущейся окр-ти до точки

    QPolygonF r1Circle; // полигон фиксированной окружности
    QPolygonF r2Circle; // полигон движущейся окружности
    QPolygonF path; // полигон траектории точки
    qreal k;    // коэф. для вычисления траектории
    qreal m;    // коэф. для вычисления траектории
    qreal xlen; // ширина прямоугольника, содержащего полигоны
    qreal ylen; // высота прямоугольника, содержащего полигоны
    const qreal tmin = 0;   // минимальный угол поворота
    const qreal tmax = 2 * M_PI;    // максимальный угол поворота
    const int iterations = 10000;   // число разбиений угла

    // Вычисление полигонов окружностей и траектории точки
    void calc(qreal t, QPointF &p1, QPointF &p2, QPointF &p3);

    // Вычисление минимального значения из трех
    qreal min3(qreal x, qreal y, qreal z)
    { return qMin(qMin(x, y), z); }

    // Вычисление максимального значения из трех
    qreal max3(qreal x, qreal y, qreal z)
    { return qMax(qMax(x, y), z); }

public:
    // Геттеры
    //
    qreal r1() const { return _r1; }
    qreal r2() const { return _r2; }
    qreal r3() const { return _r3; }

    // Установка новых параметров, вычисление полигонов, вычисление
    // ограничивающего прямоугольника
    void update(int r1, int r2, int r3);

    // Метод, вызываемый в paintEvent виджета, на котором будет
    // осуществляться отрисовка
    void paint(QPainter *p, qreal width, qreal height);

public:
    Painter() { update(60, 10, 20); }
};

#endif // PAINTER_H
